/*
Navicat MySQL Data Transfer

Source Server         : 营收平台测试（机房）
Source Server Version : 50737
Source Host           : 192.168.1.217:3306
Source Database       : role_proxy

Target Server Type    : MYSQL
Target Server Version : 50737
File Encoding         : 65001

Date: 2022-11-30 19:54:02
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for casbin_rule
-- ----------------------------
DROP TABLE IF EXISTS `casbin_rule`;
CREATE TABLE `casbin_rule` (
                               `p_type` varchar(100) DEFAULT NULL,
                               `v0` varchar(100) DEFAULT NULL COMMENT '角色id',
                               `v1` varchar(100) DEFAULT NULL COMMENT 'p_type为p表示url',
                               `v2` varchar(100) DEFAULT NULL COMMENT 'method',
                               `v3` varchar(100) DEFAULT NULL,
                               `v4` varchar(100) DEFAULT NULL,
                               `v5` varchar(100) DEFAULT NULL,
                               KEY `IDX_casbin_rule_v1` (`v1`),
                               KEY `IDX_casbin_rule_v2` (`v2`),
                               KEY `IDX_casbin_rule_v3` (`v3`),
                               KEY `IDX_casbin_rule_v4` (`v4`),
                               KEY `IDX_casbin_rule_v5` (`v5`),
                               KEY `IDX_casbin_rule_p_type` (`p_type`),
                               KEY `IDX_casbin_rule_v0` (`v0`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of casbin_rule
-- ----------------------------
INSERT INTO `casbin_rule` VALUES ('p', '1', '/v1/mod1/update', 'POST', null, null, null);
INSERT INTO `casbin_rule` VALUES ('p', '1', '/v1/role/getRoleMenus/*', 'GET', null, null, null);
INSERT INTO `casbin_rule` VALUES ('p', '1', '/v1/user/', 'GET', null, null, null);
INSERT INTO `casbin_rule` VALUES ('p', '1', '/v1/role/getUserRoles/*', 'GET', null, null, null);

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
                            `id` int(11) NOT NULL AUTO_INCREMENT,
                            `name` varchar(32) DEFAULT NULL COMMENT '模块名称',
                            `url` varchar(128) DEFAULT NULL COMMENT '模块前端地址',
                            `state` int(11) DEFAULT '1' COMMENT '菜单状态（0：禁用，1：启用）',
                            `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                            `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
                            `parent_id` int(11) DEFAULT NULL COMMENT '父菜单ID（根菜单：0）',
                            `sort` int(11) DEFAULT '0' COMMENT '排序',
                            PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '张三', null, '1', '2022-08-29 15:57:47', '2022-08-29 15:57:47', '0', '0');
INSERT INTO `sys_menu` VALUES ('2', 'bb', null, '1', '2022-11-24 19:20:19', '2022-11-24 19:20:19', '0', '0');

-- ----------------------------
-- Table structure for sys_menu_api
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu_api`;
CREATE TABLE `sys_menu_api` (
                                `id` int(11) NOT NULL AUTO_INCREMENT,
                                `title` varchar(100) DEFAULT NULL,
                                `link` varchar(100) DEFAULT NULL COMMENT '接口地址',
                                `act` varchar(20) DEFAULT NULL COMMENT '请求类型',
                                `menu_id` int(11) DEFAULT NULL COMMENT '模块id',
                                `type` tinyint(4) DEFAULT '0' COMMENT 'api类型 0查询类api 1操作类api',
                                PRIMARY KEY (`id`),
                                UNIQUE KEY `sys_menu_api_link_uindex` (`link`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_menu_api
-- ----------------------------
INSERT INTO `sys_menu_api` VALUES ('1', '模块1更新', '/v1/mod1/update', 'POST', '1', '1');
INSERT INTO `sys_menu_api` VALUES ('2', '模块1查看', '/v1/mod1/select', 'GET', '1', '0');
INSERT INTO `sys_menu_api` VALUES ('3', '模块2更新', '/v1/mod2/update', 'POST', '2', '1');
INSERT INTO `sys_menu_api` VALUES ('4', '模块2查看', '/v1/user/', 'GET', '2', '0');

-- ----------------------------
-- Table structure for sys_reg_service
-- ----------------------------
DROP TABLE IF EXISTS `sys_reg_service`;
CREATE TABLE `sys_reg_service` (
                                   `id` int(11) NOT NULL AUTO_INCREMENT,
                                   `micro_id` int(11) DEFAULT NULL COMMENT '微服务id',
                                   `code` varchar(255) DEFAULT NULL COMMENT '微服务code',
                                   `copy_num` varchar(255) DEFAULT NULL COMMENT '微服务副本编号',
                                   `service_addr` varchar(255) DEFAULT NULL COMMENT '微服务ip及端口',
                                   `note` varchar(255) DEFAULT NULL COMMENT '备注',
                                   `state` tinyint(1) DEFAULT NULL COMMENT '微服务状态 0离线1在线',
                                   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='??????';

-- ----------------------------
-- Records of sys_reg_service
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
                            `id` int(11) NOT NULL AUTO_INCREMENT,
                            `name` varchar(64) DEFAULT NULL,
                            `user_id` int(11) DEFAULT NULL COMMENT '用户id',
                            `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
                            `state` int(11) DEFAULT '1' COMMENT '角色状态 0禁用 1启用（默认）',
                            PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='???';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', 'admin', '1', '2022-09-01 16:31:05', '1');
INSERT INTO `sys_role` VALUES ('2', '高腾', '1', '2022-09-08 19:11:46', '1');
INSERT INTO `sys_role` VALUES ('3', '看看', '1', '2022-11-29 10:27:26', '1');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
                                 `id` int(11) NOT NULL AUTO_INCREMENT,
                                 `menu_id` int(11) DEFAULT NULL COMMENT '模块id',
                                 `role_id` int(11) DEFAULT NULL COMMENT '角色id',
                                 `menu_api_type` tinyint(4) DEFAULT NULL COMMENT 'api类型 0查询类api 1操作类api',
                                 PRIMARY KEY (`id`),
                                 UNIQUE KEY `sys_role_menu_pk` (`menu_id`,`role_id`),
                                 KEY `sys_role__menu_sys_role_id_fk` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('80', '1', '1', null);
INSERT INTO `sys_role_menu` VALUES ('81', '2', '1', null);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
                            `id` int(11) NOT NULL AUTO_INCREMENT,
                            `user_code` varchar(255) DEFAULT NULL,
                            `user_name` varchar(255) DEFAULT NULL,
                            `passwd` varchar(255) DEFAULT NULL,
                            `user_phone` varchar(255) DEFAULT NULL,
                            `state` int(11) DEFAULT '1' COMMENT '用户状态 1启用 0禁用',
                            PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'gt', 'gt', 'e10adc3949ba59abbe56e057f20f883e', null, '1');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
                                 `id` int(11) NOT NULL AUTO_INCREMENT,
                                 `user_id` int(11) DEFAULT NULL COMMENT '用户id',
                                 `role_id` int(11) DEFAULT NULL COMMENT '角色id',
                                 PRIMARY KEY (`id`),
                                 UNIQUE KEY `sys_role_menu_pk` (`user_id`,`role_id`),
                                 KEY `sys_role__menu_sys_role_id_fk` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1', '1');
INSERT INTO `sys_user_role` VALUES ('2', '1', '2');
