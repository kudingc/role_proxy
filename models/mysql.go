package models

type SysUser struct {
	Id        int    `json:"id"`
	UserCode  string `json:"user_code"`
	UserName  string `json:"user_name"`
	Passwd    string `json:"passwd"`
	UserPhone string `json:"user_phone"`
	State     int    `json:"state"`
}

type SysMenu struct {
	Id         int     `json:"id"`
	Name       string  `json:"name"`
	Url        *string `json:"url"`
	State      int     `json:"state"`
	CreateTime string  `json:"create_time"`
	UpdateTime string  `json:"update_time"`
	ParentId   int     `json:"parent_id"`
	Sort       int     `json:"sort"`
}

type SysMenuApi struct {
	Id     int    `json:"id"`
	Title  string `json:"title"`
	Link   string `json:"link"`
	Act    string `json:"act"`
	MenuId *int   `json:"menu_id"`
	Type   int    `json:"type"`
}

type SysRole struct {
	Id         int    `orm:"id" json:"id"`
	Name       string `orm:"name" json:"name"`
	UserId     int    `orm:"user_id" json:"user_id"`
	CreateTime string `orm:"create_time" json:"create_time"`
	State      int    `orm:"state" json:"state"`
}

type SysRoleMenu struct {
	Id          int     `json:"id"`
	MenuId      int     `orm:"menu_id" json:"menu_id"`
	RoleId      int     `orm:"role_id" json:"role_id"`
	MenuApiType ApiType `orm:"menu_api_type" json:"menu_api_type"`
}

type SysUserRole struct {
	Id     int `json:"id"`
	UserId int `orm:"user_id" json:"user_id"`
	RoleId int `orm:"role_id" json:"role_id"`
}

type SysRegService struct {
	Id          int        `json:"id"`
	MicroId     int        `json:"micro_id"`
	Code        string     `json:"code"`
	CopyNum     string     `json:"copy_num"`
	ServiceAddr string     `json:"service_addr"`
	Note        string     `json:"note"`
	State       MicroState `json:"state"`
}
