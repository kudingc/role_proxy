package models

const (
	StateOk           = 0  // 正常
	StateErr          = -1 // 错误
	StateCaptchaErr   = -2 // 验证码错误
	StateAuthorityErr = -3 // 权限错误
	StateDatabaseErr  = -4 // 数据库错误
	StateRedisErr     = -5 // Redis错误
	StateDataErr      = -6 // 数据错误
	MysqlConnErr      = "mysql connect is invalid"
	RedisConnErr      = "redis connect is invalid"
	RoleErr           = "roles is invalid"
	ParamsErr         = "params is invalid"
	ProjectNameErr    = "projectName is invalid"
	PermissionErr     = "Insufficient permissions"
)

type Res struct {
	State int         `json:"state"`
	Data  interface{} `json:"data"`
	Msg   string      `json:"msg"`
}
