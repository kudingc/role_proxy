package models

import (
	"gitee.com/kudingc/role_proxy/common"
	"github.com/astaxie/beego/orm"
	"strconv"
	"strings"
)

// 用户与角色相关操作

type RoleData struct {
	Id    int    `json:"id"`
	Name  string `json:"name"`
	State int    `json:"state"`
}

type UserData struct {
	Id       int    `json:"id"`
	UserCode string `json:"user_code"`
	UserName string `json:"user_name"`
	State    int    `json:"state"`
}

type UserRoles struct {
	UserId  int    `json:"user_id"`
	RoleIds string `json:"role_ids"`
}

type RoleUsers struct {
	RoleId  int    `json:"role_id"`
	UserIds string `json:"user_ids"`
}

// GetRoleIdsForUserId 根据用户id获取角色id列表
func (u *SysUserRole) GetRoleIdsForUserId() (roleIds string, err error) {
	sql := "select group_concat(role_id) roleIds from sys_user_role where user_id = ? group by user_id"
	err = common.Db.Raw(sql, u.UserId).QueryRow(&roleIds)
	return
}

// AddUserRoles 添加用户角色关系
func (u *SysUserRole) AddUserRoles(userId int, roleIds string, o orm.Ormer) (err error) {
	var sysUserRoleList []SysUserRole

	if o == nil {
		o = common.Db
	}

	roleIdList := strings.Split(roleIds, ",")

	for _, strRoleId := range roleIdList {
		roleId, _ := strconv.Atoi(strRoleId)
		sysUserRoleList = append(sysUserRoleList, SysUserRole{
			UserId: userId,
			RoleId: roleId,
		})
	}

	_, err = o.InsertMulti(len(sysUserRoleList), &sysUserRoleList)
	return
}

// AddRoleUsers 添加用户角色关系
func (u *SysUserRole) AddRoleUsers(roleId int, userIds string, o orm.Ormer) (err error) {
	var sysUserRoleList []SysUserRole

	if o == nil {
		o = common.Db
	}

	userIdList := strings.Split(userIds, ",")

	for _, strUserId := range userIdList {
		userId, _ := strconv.Atoi(strUserId)
		sysUserRoleList = append(sysUserRoleList, SysUserRole{
			UserId: userId,
			RoleId: roleId,
		})
	}

	_, err = o.InsertMulti(len(sysUserRoleList), &sysUserRoleList)
	return
}

// UpdateUserRoles 修改用户角色关系
func (u *SysUserRole) UpdateUserRoles(userId int, roleIds string) (err error) {
	o := orm.NewOrm()
	o.Begin()
	_, err = o.QueryTable("sys_user_role").Filter("user_id__exact", userId).Delete()
	if err != nil {
		o.Rollback()
		return
	}
	err = u.AddUserRoles(userId, roleIds, o)
	if err != nil {
		o.Rollback()
		return
	}
	o.Commit()
	return
}

// UpdateRoleUsers 修改用户角色关系
func (u *SysUserRole) UpdateRoleUsers(roleId int, userIds string) (err error) {
	o := orm.NewOrm()
	o.Begin()
	_, err = o.QueryTable("sys_user_role").Filter("role_id__exact", roleId).Delete()
	if err != nil {
		o.Rollback()
		return
	}
	err = u.AddRoleUsers(roleId, userIds, o)
	if err != nil {
		o.Rollback()
		return
	}
	o.Commit()
	return
}

// GetUserRoles 获取用户角色关系
func (u *SysUserRole) GetUserRoles(userId int) (roleDataList []RoleData, err error) {
	sql := "select distinct sr.id, sr.name, sr.state from sys_user_role sur, sys_role sr where sur.user_id = ? and sur.role_id = sr.id"
	_, err = common.Db.Raw(sql, userId).QueryRows(&roleDataList)
	return
}

// GetRoleUsers 获取角色用户关系
func (u *SysUserRole) GetRoleUsers(roleId int) (userDataList []UserData, err error) {
	sql := "select distinct su.id, su.user_code, su.user_name, su.state from sys_user_role sur, sys_user su where sur.role_id = ? and sur.user_id = su.id"
	_, err = common.Db.Raw(sql, roleId).QueryRows(&userDataList)
	return
}
