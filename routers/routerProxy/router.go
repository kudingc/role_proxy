package routerProxy

import (
	"github.com/gin-gonic/gin"
)

func Routers(engine *gin.Engine) {
	// 路由转发
	user := engine.Group("/v1/user")
	user.Any("/*action", UserProxyHeader)
}
