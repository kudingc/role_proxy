package role

import (
	"gitee.com/kudingc/role_proxy/common"
	"gitee.com/kudingc/role_proxy/service"
	"github.com/gin-gonic/gin"
)

func Routers(engine *gin.Engine) {
	//使用自定义拦截器中间件
	engine.Use(service.Authorize(common.Enforcer))

	// 修改用户与角色的关联关系
	vi := engine.Group("/v1/role")
	// ############### 角色 ###############
	// 新增角色并授权
	vi.POST("/addRole", addRoleHandler)
	// 修改角色及权限
	vi.POST("/updateRole", updateRoleHandler)
	// 获取角色信息
	vi.GET("/getRoleData/:roleId", getRoleDataHandler)
	// 获取角色列表
	vi.GET("/getRoleList", getRoleListHandler)

	// ############### 用户与角色 ###############
	// 修改用户与角色关联关系
	vi.POST("/updateUserRoles", updateUserRolesHandler)
	vi.POST("/updateRoleUsers", updateRoleUsersHandler)
	//获取用户与角色关联关系
	vi.GET("/getUserRoles/:userId", getUserRolesHandler)
	vi.GET("/getRoleUsers/:roleId", getRoleUsersHandler)

	// ############### 角色与菜单 ###############
	// 修改角色与菜单栏的对应关系
	vi.POST("/updateRoleMenus", updateRoleMenusHandler)
	// 获取角色与菜单的对应关系
	vi.GET("/getRoleMenus/:roleId", getRoleMenusHandler)

	// ############### 用户自定义api ###############
	for addr, apiData := range common.RegisteredApi {
		switch apiData.Method {
		case common.GET:
			vi.GET(addr, customFunctionHandler(apiData))
		case common.POST:
			vi.POST(addr, customFunctionHandler(apiData))

		}
	}
}
