package login

import (
	"gitee.com/kudingc/role_proxy/models"
	"gitee.com/kudingc/role_proxy/service"
	"gitee.com/kudingc/role_proxy/service/util"
	"github.com/gin-gonic/gin"
	"net/http"
)

// @Summary 用户登录
// @Tags 登录相关
// @Produce json
// @Param body body models.LoginReq true "登录信息"
// @Success 200 {object} models.Res "成功"
// @Router /v1/public/login [post]
func loginHandler(c *gin.Context) {
	var userReq models.LoginReq
	c.ShouldBind(&userReq)
	c.JSON(http.StatusOK, service.Login(userReq))
}

// @Summary 用户登出
// @Tags 登录相关
// @Produce json
// @Param Authorization header string true "token"
// @Success 200 {object} models.Res "成功"
// @Router /v1/public/logout [get]
func logoutHandler(c *gin.Context) {
	c.JSON(http.StatusOK, service.Logout(c))
}

// @Summary 获取验证码
// @Tags 登录相关
// @Produce json
// @Success 200 {object} models.Res "成功"
// @Router /v1/public/getCaptcha [get]
func getCaptchaHandler(c *gin.Context) {
	res := util.GenerateCaptchaHandler()
	c.JSON(http.StatusOK, res)
}
