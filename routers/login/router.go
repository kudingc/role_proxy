package login

import (
	"github.com/gin-gonic/gin"
)

func Routers(engine *gin.Engine) {
	// 用户登录
	vi := engine.Group("/v1/public")
	vi.POST("/login", loginHandler)
	// 用户登出
	vi.GET("/logout", logoutHandler)
	// 获取验证码
	vi.GET("/getCaptcha", getCaptchaHandler)
}
