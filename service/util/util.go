package util

import (
	"bytes"
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"golang.org/x/text/encoding/simplifiedchinese"
	"golang.org/x/text/transform"
	"io/ioutil"
	"math/rand"
	"mime/multipart"
	"os"
	"path"
	"strings"
	"time"
	"unicode"
)

// GetMd5String 密码加密
func GetMd5String(b []byte) string {
	return fmt.Sprintf("%x", md5.Sum(b))
}

// CheckPhoneNo 检查电话号码格式
func CheckPhoneNo(phoneNo string) bool {
	if len(phoneNo) == 0 {
		return true
	}
	if len(phoneNo) != 11 {
		return false
	}
	return isDigit(phoneNo)
}

// CheckId 检查id，即判断id字符串是否全部由数字组成
func CheckId(id string) bool {
	return isDigit(id)
}

func isDigit(str string) bool {
	for _, x := range []rune(str) {
		if !unicode.IsDigit(x) {
			return false
		}
	}
	return true
}

// CheckUserName 检查用户名，可以是数字，可以是英文字母，可以是汉字，不能为其它的。如果是空字符串，返回false，因为userName是必须要有的
func CheckUserName(strUserName string) (ok bool) {
	for _, letter := range strUserName {
		if unicode.IsLetter(letter) || unicode.IsDigit(letter) {
			ok = true
		} else {
			ok = false
			return
		}
	}
	return
}

// PathExists 检查目录是否存在
func PathExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil { //⽂件或者⽬录存在
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

// MD5 Md5加密
func MD5(v string) string {
	d := []byte(v)
	m := md5.New()
	m.Write(d)
	return hex.EncodeToString(m.Sum(nil))
}

// RemoveSymbol 移除尾部逗号
func RemoveSymbol(str string) string {
	for str != "" && string([]byte(str)[len(str)-1]) == "," {
		str = strings.TrimRight(str, ",")
	}
	return str
}

// GetDateStringArr 获取年月日
func GetDateStringArr(t time.Time) (year string, month string, day string) {
	year = t.Format("2006")
	month = t.Format("01")
	day = t.Format("02")
	return
}

// CheckUploadImagesExt 检查上传图片后缀是否符合要求，不符合返回true
func CheckUploadImagesExt(files []*multipart.FileHeader) bool {
	var AllowExtMap map[string]bool = map[string]bool{
		".jpg":  true,
		".jpeg": true,
		".png":  true,
		".gif":  true,
	}

	for _, file := range files {
		if _, ok := AllowExtMap[path.Ext(file.Filename)]; !ok {
			return true
		}
	}
	return false
}

// GenerateRandFileName 生成随机文件名
func GenerateRandFileName() string {
	rand.Seed(time.Now().UnixNano())
	randNum := fmt.Sprintf("%d", rand.Intn(9999)+1000)
	hashName := md5.Sum([]byte(time.Now().Format("2006_01_02_15_04_05_") + randNum))
	return fmt.Sprintf("%x", hashName)
}

func CheckUserAgent(userAgent string) int {
	keywords := []string{"Android", "iPhone", "iPod", "iPad", "Windows Phone", "MQQBrowser"}

	for i := 0; i < len(keywords); i++ {
		if strings.Contains(userAgent, keywords[i]) {
			return 2
		}
	}
	return 1
}

// GetAscii 根据数字获取英文ASCII码 0对应A,超过Z后，从AA开始
func GetAscii(i int) (s string) {
	var r rune = rune(i + 65)
	s = string(r)
	if r > 90 {
		s = "A" + string(r-90+64)
	}
	return
}

func GbToUtf8(s []byte) []byte {
	//reader := transform.NewReader(byte.NewReader(s), simplifiedchinese.GBK.NewEncoder())
	reader := transform.NewReader(bytes.NewReader(s), simplifiedchinese.GBK.NewDecoder())
	d, e := ioutil.ReadAll(reader)
	if e != nil {
		return nil
	}
	return d
}
