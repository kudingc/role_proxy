package util

import (
	"gitee.com/kudingc/role_proxy/common"
	"gitee.com/kudingc/role_proxy/models"
	"strings"
)

// ErrHandle 统一错误处理
func ErrHandle(err error, v ...interface{}) (state int, strErr string) {
	strErr = err.Error()

	common.ZhLogger.Error(v, err)

	switch {
	case strings.Index(strErr, "invalid connection") >= 0:
		return models.StateDatabaseErr, "数据库连接失败，请联系管理员！"
	case strings.Index(strErr, "Duplicate entry") >= 0:
		return models.StateDatabaseErr, "字段“" + strings.Split(strErr, "'")[1] + "”已存在！"
	case strings.Index(strErr, "Cannot delete or update a parent row") >= 0:
		return models.StateDatabaseErr, "当前数据下还有关联数据，无法删除"
	case strings.Index(strErr, "forcibly closed") >= 0:
		return models.StateRedisErr, "Redis连接失败，请联系管理员"
	case strings.Index(strErr, "no row found") >= 0:
		return models.StateDatabaseErr, "操作数据不存在"
	case strings.Index(strErr, "foreign key constraint fails") >= 0:
		return models.StateDatabaseErr, "当前数据父字段不存在，请刷新页面后重试"
	case strings.Index(strErr, "Data too long") >= 0:
		return models.StateDatabaseErr, "字段“" + strings.Split(strErr, "'")[1] + "”过长，已超过最大限制！"
	}
	return models.StateErr, strErr
}
