package util

import (
	"errors"
	"gitee.com/kudingc/role_proxy/common"
	"time"
)

func RedisSetStr(key, value string) (err error) {
	if common.RedisCache == nil {
		return errors.New("redis connect is invalid")
	}
	return common.RedisCache.Put(key, value, time.Duration(common.TokenExpirationTime)*time.Minute)
}

func RedisGetStr(key string) (value string, err error) {
	if common.RedisCache == nil {
		return "", errors.New("redis connect is invalid")
	}
	v := common.RedisCache.Get(key)
	if v == nil {
		return
	}
	value = string(v.([]byte)) //这里的转换很重要，Get返回的是interface
	return
}

func RedisIsExist(key string) (ok bool, err error) {
	if common.RedisCache == nil {
		return false, errors.New("redis connect is invalid")
	}
	return common.RedisCache.IsExist(key), nil
}

func RedisDelKey(key string) (err error) {
	if common.RedisCache == nil {
		return errors.New("redis connect is invalid")
	}
	err = common.RedisCache.Delete(key)
	return
}
