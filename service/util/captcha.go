package util

import (
	"gitee.com/kudingc/role_proxy/models"
	"github.com/mojocn/base64Captcha"
)

//configJsonBody json request body.
type configJsonBody struct {
	Id            string
	CaptchaType   string
	VerifyValue   string
	DriverAudio   *base64Captcha.DriverAudio
	DriverString  *base64Captcha.DriverString
	DriverChinese *base64Captcha.DriverChinese
	DriverMath    *base64Captcha.DriverMath
	DriverDigit   *base64Captcha.DriverDigit
}

var store = base64Captcha.DefaultMemStore

// GenerateCaptchaHandler base64Captcha create http handler
func GenerateCaptchaHandler() models.Res {
	var driver base64Captcha.Driver
	// 配置验证码的参数
	//driverString := base64Captcha.DriverString{
	//	Height:          40,
	//	Width:           100,
	//	NoiseCount:      0,
	//	ShowLineOptions: 2 | 4,
	//	Length:          4,
	//	Source:          "1234567890qwertyuioplkjhgfdsazxcvbnm",
	//	BgColor:         &color.RGBA{R: 255, G: 255, B: 255, A: 255},
	//	Fonts:           []string{"wqy-microhei.ttc"},
	//}
	// ConvertFonts 按名称加载字体
	//driver = driverString.ConvertFonts()
	driver = base64Captcha.DefaultDriverDigit
	c := base64Captcha.NewCaptcha(driver, store)
	id, b64s, err := c.Generate()

	if err != nil {
		return models.Res{
			State: models.StateErr,
			Msg:   err.Error(),
		}
	}

	return models.Res{
		State: models.StateOk,
		Msg:   "success",
		Data:  map[string]interface{}{"captcha_data": b64s, "captcha_id": id},
	}
}

// CaptchaVerifyHandle base64Captcha verify http handler
func CaptchaVerifyHandle(captchaId string, captchaValue string) (ok bool) {
	//verify the captcha
	ok = false
	if captchaValue == "" {
		return
	}
	if store.Verify(captchaId, captchaValue, true) {
		ok = true
	}
	return
}
