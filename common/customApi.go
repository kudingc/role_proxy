package common

type Method int

const (
	GET Method = iota
	POST
)

type PathType int

const (
	BODY PathType = iota
	PATH
	QUERY
)

type ApiData struct {
	ApiAddr string                               // api地址
	Method  Method                               // api请求method 例 get post
	F       func() (data interface{}, err error) // 回调函数
	Params  map[string]*Param                    // api参数
}

type Param struct {
	PathType   PathType    // 参数路径类型
	ParamName  string      // 参数名称
	ParamValue interface{} // 参数值，用于保存入参
}
