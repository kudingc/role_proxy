package common

import (
	"gitee.com/kudingc/zh_logs/logging"
	"github.com/astaxie/beego/cache"
	"github.com/astaxie/beego/orm"
	"github.com/casbin/casbin"
	xormadapter "github.com/casbin/xorm-adapter"
)

var (
	RegisteredApi       = make(map[string]ApiData)
	Adapter             *xormadapter.Adapter // 规则数据库连接对象
	Enforcer            *casbin.Enforcer     // 规则对象
	Db                  orm.Ormer            // MySQL数据库对象
	RedisCache          cache.Cache          // Redis连接对象
	ZhLogger            logging.ZhLogger     // 日志对象
	CheckTokenExpired   bool                 // 是否检查token过期
	TokenExpirationTime int                  // token过期时间
	TokenEncryptionKey  string               // token加密key
	CheckCaptcha        bool                 // 是否检查验证码
)
