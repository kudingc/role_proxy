FROM tetsuya693/centos7.9:20210202

# 创建文件夹
RUN mkdir /app

#将Dockerfile 中的文件存储到/app下
ADD . /app/

# 设置工作目录
WORKDIR ./app

# 暴露的端口
EXPOSE 9000

# 执行程序existed_meter_data_service

ENTRYPOINT ["./meter_manage_platform"]