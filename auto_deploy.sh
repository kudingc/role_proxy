#!/bin/bash

program=meter_manage_platform
deploy_path=/root/docker-program/meter_manage_platform/

bee pack -be GOOS=linux

#登录用户名
name="root"
# dev hosts
deploy_server="36.134.44.60"

scp -v -P 10012 ${program}.tar.gz ${name}@${deploy_server}:${deploy_path}${program}.tar.gz

ssh ${name}@${deploy_server} -p 10012 "cd $deploy_path && tar -xzvf ${program}.tar.gz && chmod 777 ${program}"

ssh ${name}@${deploy_server} -p 10012 "docker restart ${program}"

exit;